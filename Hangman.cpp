#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
#include <algorithm>
#include <unordered_set>

std::string getWord() {
	std::ifstream fin("cuvinte.txt");
	int number = rand() % 634313;
	std::string word;
	for (int index = 0; index < number; ++index) {
		fin >> word;
	}
	fin.close();
	return word;
}

std::string constructWord(const std::string& initialWord) {
	std::string underScoreWord;
	for (int index = 0; index < initialWord.length(); ++index) {
		underScoreWord.append("_ ");
	}
	underScoreWord.pop_back();
	return underScoreWord;
}

void playTheGame(const std::string& Word, std::string& underScoreWord,
				 std::string& intermediateWord) {
	int noOfTrials = 0;
	std::string initialWord(Word);
	std::cout << "You have 10 attemps to guess the word.\nIntroduce one letter at a time.\n";
	std::string letter;
	std::string symbol = "#";
	std::unordered_set<std::string> usedLetters;
	while (intermediateWord != initialWord && noOfTrials != 10) {
		if (usedLetters.size() != 0) {
			std::cout << "Used letters are: ";
			for (auto it : usedLetters) {
				std::cout << it << ", ";
			}
		}
		std::cout << "\n" << underScoreWord << "\nEnter letter: ";
		while (std::cin >> letter && letter.length() != 1) {
			std::cout << "Invalid string, please repeat\n";
		}
		std::transform(letter.begin(), letter.end(), letter.begin(), ::tolower);
		usedLetters.insert(letter);
		if (initialWord.find(letter) == std::string::npos) {
			noOfTrials++;
			std::cout << "You entered a wrong letter. You now have " << 10 - noOfTrials << " more attempts.\n";
		}
		else {
			while (initialWord.find(letter, 0) != std::string::npos) {
				int index = initialWord.find(letter);
				underScoreWord.replace(index * 2, 1, letter);
				initialWord.replace(index, 1, symbol);
			}
		}
	}
	if (noOfTrials == 10) {
		std::cout << "You unfortunately lost the game.\nThe word was: " << Word;
	}
	else {
		std::cout << "Congratulations you've won the game.";
	}
}

int main() {
	std::string input = "YES";
	while (input == "YES") {
		srand((unsigned) time(NULL));
		std::string initialWord = getWord(),
			underScoreWord = constructWord(initialWord),
			intermediateWord(initialWord.length(), '#');
		playTheGame(initialWord, underScoreWord, intermediateWord);
		std::cout << "Do you want to play again? (YES/NO) ";
		std::cin >> input;
		if (input == "NO") {
			std::cout << "\nThank you for playing!\n";
			return 0;
		}
	}
	return 0;
}