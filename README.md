# Hangman

Hangman is a guessing word game, where a word is selected from a list and all the letters are hidden.\ The player has to guess letters from the word until he uses all his tries(10) or he finds the correct word.\ I implemented this as a way to consolidate my C++ STL and String knowledge.
In the future I will probably try to implement it in an OOP manner with a GUI.

## Installation
Just clone the repository and compile the .cpp.

## Usage
Enter letters(the case is not important) until you guess the word or you finish your tries.

## Implementation
For implementing this I used the C++ Standard Template Library.
## Planned Features
Rewrite the program in OOP manner.\
Implement GUI interface.\
